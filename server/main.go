package main

import "net/http"

func main() {
	queue := &Queue{
		Transport: PostTransport{
			Host:        "http://localhost:8081",
			ContentType: "application/json",
		},
		NumWorkers:   3,
		ChanBuffSize: 1000,
	}

	app := &App{Queue: queue}
	app.Run()

	http.HandleFunc("/get", app.GetHandler)
	http.HandleFunc("/status", app.StatusHandler)

	http.ListenAndServe(":8080", nil)
}
