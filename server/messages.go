package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

type CommandMessage struct {
	Command string      `json:"cmd"`
	Key     string      `json:"key"`
	Value   interface{} `json:"val"`
}

type CommandResult struct {
	Value interface{} `json:"val"`
}

type CommandRemoteError struct {
	Error string `json:"err"`
}

func (message CommandMessage) Serialize(out io.Writer) {
	if err := json.NewEncoder(out).Encode(message); err != nil {
		log.Panic("err", err)
	}
}

func (message CommandMessage) Response(code int, in io.Reader) (val interface{}, err error) {
	bs, _ := ioutil.ReadAll(in)
	//fmt.Println(string(bs))
	in = bytes.NewReader(bs)
	decoder := json.NewDecoder(in)
	if code == http.StatusOK || code == http.StatusCreated {
		val = &CommandResult{}
	} else {
		val = &CommandRemoteError{}
	}
	err = decoder.Decode(&val)
	return val, err
}
