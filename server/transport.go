package main

import "io"
import "net/http"
import "fmt"

type Transport interface {
	Send(in io.Reader) (int, io.ReadCloser, error)
}

type PostTransport struct {
	Host        string
	ContentType string
}

func (transport PostTransport) Send(in io.Reader) (int, io.ReadCloser, error) {
	response, err := http.Post(transport.Host, transport.ContentType, in)
	if response.StatusCode >= 400 && response.StatusCode < 500 {
		return response.StatusCode, nil, fmt.Errorf("Transport error: %s", response.Status)
	}
	return response.StatusCode, response.Body, err
}
