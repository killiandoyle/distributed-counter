package main

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"sync"
)

type App struct {
	Queue         *Queue
	lock          sync.Locker
	numGetHandles int64
}

func (app *App) Run() *App {
	app.lock = new(sync.Mutex)
	app.numGetHandles = 0
	app.Queue.Run()
	return app
}

func calulateDelta(n int64) int {
	if math.Mod(math.Floor(float64((n)/10)), 2) == 1 {
		return -1
	}
	return 1
}

func (app *App) GetHandler(w http.ResponseWriter, r *http.Request) {
	app.lock.Lock()
	delta := calulateDelta(app.numGetHandles)
	app.numGetHandles++
	app.lock.Unlock()

	app.Queue.Add(CommandMessage{
		Command: "ADD",
		Key:     "counter",
		Value:   delta,
	})

	w.Write([]byte("request sent"))
}

func (app *App) StatusHandler(w http.ResponseWriter, r *http.Request) {
	recv := app.Queue.Add(CommandMessage{
		Command: "GET",
		Key:     "counter",
	})
	switch value := <-recv; value := value.(type) {
	case *CommandResult:
		fmt.Fprint(w, value.Value)
	case *CommandRemoteError:
		w.WriteHeader(http.StatusBadRequest)
		fmt.Print(w, value.Error)
	case error:
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Print(w, value.Error())
	default:
		log.Panic("Should not be here", value)
	}
}
