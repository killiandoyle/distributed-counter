package main

import (
	"bytes"
	"io"
)

type Message interface {
	Serialize(io.Writer)
	Response(code int, in io.Reader) (interface{}, error)
}

type queueItem struct {
	Message   Message
	Responder chan interface{}
	Response  interface{}
}

type Queue struct {
	Transport    Transport
	NumWorkers   int
	ChanBuffSize int

	in      chan *queueItem
	out     chan *queueItem
	running bool
}

func (queue *Queue) Run() {
	if queue.running {
		return
	}
	if queue.ChanBuffSize == 0 {
		queue.ChanBuffSize = 999
	}
	if queue.NumWorkers < 1 {
		queue.NumWorkers = 1
	}

	queue.in = make(chan *queueItem, queue.ChanBuffSize)
	queue.out = make(chan *queueItem, queue.ChanBuffSize)
	for i := 0; i < queue.NumWorkers; i++ {
		go worker(queue.Transport, queue.in, queue.out)
	}

	go func() {
		for {
			item := <-queue.out
			item.Responder <- item.Response
			close(item.Responder)
		}
	}()
}

func (queue *Queue) Add(message Message) chan interface{} {
	item := &queueItem{
		Message:   message,
		Responder: make(chan interface{}, 1),
	}
	queue.in <- item
	return item.Responder
}

func worker(transport Transport, in, out chan *queueItem) {
	for {
		item := <-in
		msg := item.Message
		buff := bytes.NewBuffer(nil)
		msg.Serialize(buff)
		if code, response, err := transport.Send(buff); err != nil {
			item.Response = err
		} else if value, err := msg.Response(code, response); err != nil {
			item.Response = err
			response.Close()
		} else {
			item.Response = value
			response.Close()
		}
		out <- item
	}
}
