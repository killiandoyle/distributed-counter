package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

type TestQueueTransport struct {
}

func (transport TestQueueTransport) Send(in io.Reader) (int, io.ReadCloser, error) {
	request := &CommandMessage{}
	json.NewDecoder(in).Decode(&request)
	buff := bytes.NewBuffer(nil)
	json.NewEncoder(buff).Encode(CommandResult{
		Value: request.Value,
	})
	return http.StatusOK, ioutil.NopCloser(buff), nil
}

func TestQueue(t *testing.T) {
	queue := &Queue{
		Transport:    TestQueueTransport{},
		NumWorkers:   1,
		ChanBuffSize: 1,
	}
	queue.Run()
	type Err struct {
		error
	}
	cases := []CommandMessage{
		{"SET", "test", 0.0},
		{"ADD", "test", 1.0},
		{"GET", "test", 0.0},
	}

	var responses []chan interface{}
	for _, c := range cases {
		responses = append(responses, queue.Add(c))
	}

	for i, r := range responses {
		c := cases[i]
		select {
		case rcv := <-r:
			switch rcv := rcv.(type) {
			case *CommandResult:
				if c.Value != rcv.Value {
					t.Error("Do not match", c.Value, rcv.Value)
				}
			default:
				t.Errorf("no match %T", rcv)
			}
		case <-time.After(time.Second):
			t.Fatal("No result on channel")
		}
	}
}
