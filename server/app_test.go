package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

type TestServerTransport struct {
	Counter float64
}

func (transport *TestServerTransport) Send(in io.Reader) (int, io.ReadCloser, error) {
	request := &CommandMessage{}
	json.NewDecoder(in).Decode(&request)
	buff := bytes.NewBuffer(nil)
	switch request.Command {
	case "GET":
	case "ADD":
		transport.Counter = transport.Counter + request.Value.(float64)
	}
	json.NewEncoder(buff).Encode(CommandResult{
		Value: transport.Counter,
	})
	return http.StatusOK, ioutil.NopCloser(buff), nil
}

func TestCounter(t *testing.T) {
	cases := []struct {
		numGets       int
		expectedCount float64
	}{{10, 10}, {100, 0}, {112, 8}}

	for _, c := range cases {
		app := &App{Queue: &Queue{
			Transport:    &TestServerTransport{0},
			NumWorkers:   1,
			ChanBuffSize: 1,
		}}
		app.Run()
		responses := make(chan bool, c.numGets)
		for i := 0; i < c.numGets; i++ {
			go func() {
				rr := httptest.NewRecorder()
				getHandler := http.HandlerFunc(app.GetHandler)
				req, _ := http.NewRequest("GET", "/get", nil)
				getHandler.ServeHTTP(rr, req)
				responses <- true
			}()
		}
		n := 0
		for n < c.numGets && <-responses {
			n++
		}
		statusHandler := http.HandlerFunc(app.StatusHandler)
		req, _ := http.NewRequest("GET", "/status", nil)
		rr := httptest.NewRecorder()
		statusHandler.ServeHTTP(rr, req)
		bs, _ := ioutil.ReadAll(rr.Body)
		count, _ := strconv.ParseFloat(string(bs), 64)
		if count != c.expectedCount {
			t.Error("Got", count, "expected", c.expectedCount)
		}
	}
}

func TestCalculateDelta(t *testing.T) {
	cases := []struct {
		n        []int64
		expected int
	}{
		{[]int64{0, 1, 9, 20}, 1},
		{[]int64{10, 11, 19, 30}, -1},
	}
	for _, c := range cases {
		for _, i := range c.n {
			delta := calulateDelta(i)
			if c.expected != delta {
				t.Error("For", i, "got", delta, "expected", c.expected)
			}
		}
	}
}
