package main

import (
	"sync"
)

type Memory struct {
	values map[string]interface{}
	lock   *sync.RWMutex
}

func NewMemory() *Memory {
	return &Memory{
		make(map[string]interface{}),
		new(sync.RWMutex),
	}
}

func (memory *Memory) Get(key string) (interface{}, bool) {
	memory.lock.RLock()
	defer memory.lock.RUnlock()
	value, found := memory.values[key]
	return value, found
}

func (memory *Memory) Set(key string, value interface{}) {
	memory.lock.Lock()
	defer memory.lock.Unlock()
	memory.values[key] = value
}

func (memory *Memory) Modify(key string, fn func(value interface{}, found bool) interface{}) interface{} {
	memory.lock.Lock()
	defer memory.lock.Unlock()
	value, found := memory.values[key]
	memory.values[key] = fn(value, found)
	return memory.values[key]
}
