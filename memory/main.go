package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type ResponseError struct {
	HTTPStatus int    `json:"status"`
	Error      string `json:"error"`
}

func writeError(w http.ResponseWriter, httpStatus int, err string) {
	w.WriteHeader(httpStatus)
	writeJSON(w, ResponseError{httpStatus, err})
}

func writeJSON(w http.ResponseWriter, value interface{}) error {
	return json.NewEncoder(w).Encode(value)
}

type WebHandler struct {
	memory *Memory
}

func (handler *WebHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var args struct {
		Command string      `json:"cmd"`
		Key     string      `json:"key"`
		Value   interface{} `json:"val"`
	}
	defer r.Body.Close()
	if err := json.NewDecoder(r.Body).Decode(&args); err != nil {
		writeError(w, http.StatusInternalServerError, err.Error())
		return
	}
	switch args.Command {
	case "GET":
		if value, found := handler.memory.Get(args.Key); !found {
			writeError(w, http.StatusNotFound, "Key not found")
		} else {
			writeJSON(w, struct {
				Value interface{} `json:"val"`
			}{value})
		}
	case "SET":
		handler.memory.Set(args.Key, args.Value)
		writeJSON(w, struct {
			Value interface{} `json:"val"`
		}{args.Value})
	case "ADD":
		value := handler.memory.Modify(args.Key, func(v interface{}, found bool) interface{} {
			var number float64
			var isNumber bool
			if number, isNumber = v.(float64); !isNumber || !found {
				number = 0
			}
			return number + args.Value.(float64)
		})
		writeJSON(w, struct {
			Value interface{} `json:"val"`
		}{value})
	default:
		writeError(w, http.StatusNotFound, "Command not found")
	}
}

func main() {
	handler := &WebHandler{
		NewMemory(),
	}
	mux := http.NewServeMux()
	mux.Handle("/", handler)
	server := http.Server{Handler: mux, Addr: ":8081"}
	log.Fatal(server.ListenAndServe())
}
