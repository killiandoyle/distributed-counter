package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestServerNumbers(t *testing.T) {
	handler := &WebHandler{NewMemory()}
	server := httptest.NewServer(handler)
	defer server.Close()

	cases := []struct {
		Command  string  `json:"cmd"`
		Key      string  `json:"key"`
		Value    float64 `json:"val"`
		expected float64
	}{
		{"SET", "1", 1, 1},
		{"GET", "1", 1, 1},
		{"ADD", "1", 1, 2},
		{"ADD", "1", -1, 1},
	}

	for _, c := range cases {
		inBody := bytes.NewBuffer(nil)
		json.NewEncoder(inBody).Encode(c)
		resp, err := http.Post(server.URL, "application/json", inBody)
		if err != nil {
			t.Fatal(err)
		} else if resp.StatusCode != 200 {
			t.Fatalf("Received non-200 response: %d\n", resp.StatusCode)
		}
		var outBody struct {
			Value float64 `json:"val"`
		}
		if err := json.NewDecoder(resp.Body).Decode(&outBody); err != nil {
			t.Fatal("could not decode body")
		}
		if outBody.Value != c.expected {
			t.Error("Expected", c.expected, "got", outBody.Value)
		}
	}
}
