package main

import "testing"

func TestNumbers(t *testing.T) {
	cases := []struct {
		key         string
		value, want int
	}{
		{"0", 0, 0},
	}
	memory := NewMemory()
	for _, c := range cases {
		memory.Set(c.key, c.value)
		if actual, found := memory.Get(c.key); actual != c.want || !found {
			t.Failed()
		}
	}
}

func TestModifyNumbers(t *testing.T) {
	cases := []struct {
		key          string
		value, delta float64
		want         float64
	}{
		{"0", 0, 1, 1},
		{"", 0, -1, -1},
		{"", 0, 0, 0},
	}
	memory := NewMemory()
	for _, c := range cases {
		memory.Set(c.key, c.value)
		memory.Modify(c.key, func(current interface{}, found bool) interface{} {
			return current.(float64) + c.delta
		})
		if actual, found := memory.Get(c.key); actual.(float64) != c.want || !found {
			t.Error("Failed")
		}
	}
}
